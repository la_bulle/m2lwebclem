const pg = require('pg');

exports.getAllActivites=function(req, res, next) {

    if (req.session.user === undefined)
        res.redirect('/');
    else
    {
        const connectionString = 'postgres://postgres:Password1@192.168.1.50:5432/M2LWeb';

        const db = new pg.Client(connectionString);
        db.connect();


        const query = {
            name: 'fetch-all-activite',
            text: 'SELECT * FROM activite'
        };

        db.query(query,
            function (err, result) {
                if (err) {
                    console.log(err.stack);
                    res.send('ERROR BD');

                } else {
                    res.render('activite', {listeDesActivites: result.rows,user:req.session.user});
                }
                db.end();
            }
        );
    }
};