var express = require('express');
var router = express.Router();

var controllerIndex = require("../controllers/controllerIndex");
var controllerActivite = require("../controllers/controllerActivite");

/* GET Connexion page. */
router.get('/', controllerIndex.connexion);
router.post('/', controllerIndex.verificationConnexion);

router.get('/logout', controllerIndex.deconnexion);

router.get('/index', controllerIndex.index);

router.get('/profile', controllerIndex.getProfilUser);


/* ACTIVITES*/
router.get('/activites',controllerActivite.getAllActivites);






module.exports = router;
